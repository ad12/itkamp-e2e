var basePath = __dirname;
exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    allScriptsTimeout: 100000,
    suites: {
        login: ["tests_spec/loginSpec.js"],
    },
    params: {
        config: {
            language: "PL" // default
        },
    },
    onPrepare: function () {
        if (browser.params.config.language === "PL") {

        }
        else if (browser.params.config.language === "RO") {

        }
        else {

        }
        browser.get("http://www.fru.pl");
        browser.driver.manage().window().maximize();
    },
    multiCapabilities: [
        {'browserName': 'chrome'}
        // {'browserName': 'firefox'}
    ]

};
